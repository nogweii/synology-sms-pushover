require 'json'
require 'excon'

# Passed a WEBrick::HTTPRequest and WEBrick::HTTPResponse, respond to the
# incoming request as appropriate.
Handler = Proc.new do |web_req, web_res|
  status_code = 0
  response_body = ""

  headers = web_req.header

  if headers['x-api-key'][0] != ENV['API_KEY']
    status_code = 401
    response_body = "No API sent in X-API-Key. Or the wrong one. Go fix your configuration!"
  elsif headers['content-type'][0] != 'application/json'
    status_code = 406
    response_body = "Content not labeled as application/json, rejecting your request."
  else
    body = JSON.parse(web_req.body)

    message = body['message']

    res = Excon.post("https://api.pushover.net/1/messages.json",
              :body => URI.encode_www_form({
                token: ENV['PUSHOVER_API_KEY'],
                user: ENV['PUSHOVER_USER_KEY'],
                message: message,
              }),
              :headers => { "Content-Type" => "application/x-www-form-urlencoded" })

    status_code = 200
    response_body = "Message sent to Pushover."
  end

  # The response is always plain text, 'cause I'm lazy
  web_res['Content-Type'] = 'text/plain'
  web_res.status = status_code
  web_res.body = response_body
end
