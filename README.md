# synology-sms-pushover

A simple webhook that forwards messages sent from a Synology NAS via the "SMS
Notification" to Pushover. Built for Zeit Now serverless functions.


# Configuring your NAS

There is an API key saved as a secret (and exposed as an environment variable).
It's required to ensure that no random messages get sent.

## Add SMS service provider
Go to the Control Panel, then in the Notification Section, go to the SMS tab.
Enable SMS notifications, and then click on the button 'Add SMS service provider'.

#### First page of the wizard

* Provider name: Whatever you want
* SMS URL: https://synology-sms-pushover.nogweii.now.sh/api/sms
* HTTP Method: POST

#### Create some request headers

* Parameter: X-API-Key, Value: *(none)*

#### Create the request body parameters

* Parameter: message, Value: hello world
* Parameter: phone, Value: *(none)*

#### Select the categories for each parameter

| Parameter        | Category  |
| ------------- |---------------|
| X-API-Key   | API Key |
| message      | Message Content |
| phone | Phone number  |
